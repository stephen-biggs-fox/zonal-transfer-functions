"""
Class responsible for calculation of bi-coherence as a function of theta

Copyright 2021 Steve Biggs

This file is part of zonal-flow-drive.

zonal-flow-drive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

zonal-flow-drive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with zonal-flow-drive.  If not, see http://www.gnu.org/licenses/.
"""


from overrides import overrides
from BicoherenceCalculator import BicoherenceCalculator
from SpectrumCalculator import SpectrumCalculator
from FunctionCalculator import FunctionCalculator
import numpy as np


# This class operates like a function in that it parallelises over theta, only
# calculates the coupling between zonal flows and turbulence (not turbulence-turbulence
# coupling), etc. but it outputs the spectrum so has to inherit SpectrumCalculator and
# FunctionCalculator.

# Order is important as there are clashes between SpectrumCalculator and
# FunctionCalculator but we want to favor SpectrumCalculator so that has to be before
# FunctionCalculator
class BicoherenceSpectrumCalculator(
    BicoherenceCalculator, SpectrumCalculator, FunctionCalculator
):
    @overrides
    def getOutputVariableDims(self):
        return ("theta", "ky_prime", "kx_prime", "kx")
