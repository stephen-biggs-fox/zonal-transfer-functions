#! /usr/bin/env python3
"""
Script to compute nonlinear transfer function

Copyright 2020 Steve Biggs

This file is part of initial.

initial is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

initial is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with initial.  If not, see http://www.gnu.org/licenses/.
"""

import argparse
import multiprocessing as mp
import numpy as np
import sys
from TransferFunctionCalculator import TransferFunctionCalculator
from TransferSpectrumCalculator import TransferSpectrumCalculator
from BicoherenceFunctionCalculator import BicoherenceFunctionCalculator
from BicoherenceSpectrumCalculator import BicoherenceSpectrumCalculator
import warnings
import xarray as xr


def parse_args():
    class UltimateHelpFormatter(
        argparse.RawTextHelpFormatter, argparse.ArgumentDefaultsHelpFormatter
    ):
        pass

    parser = argparse.ArgumentParser(
        description="Compute nonlinear transfer function and related quantities",
        formatter_class=UltimateHelpFormatter,
    )
    parser.add_argument(
        "-i",
        "--input",
        default="input.out.nc",
        help="input filename",
    )
    parser.add_argument(
        "-o",
        "--output",
        default="transfer.nc",
        help="output filename",
    )
    parser.add_argument(
        "-s",
        "--start",
        type=float,
        default=0.0,
        help="start time",
    )
    parser.add_argument("-e", "--end", type=float, help="end time")
    parser.add_argument(
        "-n",
        "--nproc",
        type=int,
        default=1,
        help="number of processors",
    )
    parser.add_argument(
        "-m",
        "--mode",
        choices=["tf", "ts", "bf", "bs"],
        default=["tf", "ts"],
        nargs="+",
        help=(
            "mode of operation (tf = transfer function, ts = transfer spectrum,\n"
            + " bf = bicoherence function, bs = bicoherence spectrum)\n"
        ),
    )
    all_type_choices = ["v", "n", "d"]
    type_default = all_type_choices
    parser.add_argument(
        "-t",
        "--type",
        choices=all_type_choices,
        default=type_default,
        nargs="+",
        help=("type of transfer (v = velocity, n = ntot, d = density)\n"),
    )
    parser.add_argument(
        "-th",
        "--theta",
        type=float,
        default=0.0,
        help=("theta at which to calculate spectrum (only valid in ts mode)"),
    )
    parser.add_argument(
        "-x",
        "--index",
        action="store_true",
        help=(
            "if set, then start, end and theta are interpreted at indexes rather\n"
            + "than actual values in simulation units"
        ),
    )
    args = parser.parse_args()
    return args


def new_calculator(mode):
    if mode == "tf":
        calculator = TransferFunctionCalculator(args)
    elif mode == "ts":
        calculator = TransferSpectrumCalculator(args)
    elif mode == "bf":
        calculator = BicoherenceFunctionCalculator(args)
    elif mode == "bs":
        calculator = BicoherenceSpectrumCalculator(args)
    else:
        raise ValueError("Unrecognised mode of operation '{}'".format(mode))
    return calculator


if __name__ == "__main__":
    # Parse command line inputs
    args = parse_args()
    # Instantiate calculator of correct type
    for mode in args.mode:
        print("Running " + mode + " calculator...")
        calculator = new_calculator(mode)
        # Perform calculations
        calculator.calculate()
    print("Done!")
