"""
Abstract base class to represent common aspects of nonlinear coupling functions of theta

Copyright 2021 Steve Biggs

This file is part of zonal-flow-drive.

zonal-flow-drive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

zonal-flow-drive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with zonal-flow-drive.  If not, see http://www.gnu.org/licenses/.
"""

from overrides import overrides
from NonlinearCouplingCalculator import NonlinearCouplingCalculator
import numpy as np
import utils


class FunctionCalculator(NonlinearCouplingCalculator):
    @overrides
    def getConjShape(self):
        #      ky',kx',     kx,       t
        return (1, 1, self.nkx, self.nt)

    @overrides
    def getMediatorShape(self):
        #             ky',      kx',      kx,       t
        return (self.nky, self.nkx, self.nkx, self.nt)

    @overrides
    def getSourceShape(self):
        #             ky',      kx',kx,      t
        return (self.nky, self.nkx, 1, self.nt)

    @overrides
    def getParallelDim(self):
        return "theta"

    @overrides(check_signature=False)
    def get_t_slice(self, _):
        return slice(None)

    @overrides
    def get_theta_slice(self, i):
        return i

    @overrides
    def getArrayPreparationFunctions(self):
        prepFns = super().getArrayPreparationFunctions()
        prepFns.insert(1, utils.move_first_axis_to_last)
        return prepFns

    @overrides
    def getMakeFullSlice(self):
        return super().getMakeFullSlice() + (slice(None),)

    @overrides
    def compute_phi_m(self, phi):
        # Pre-allocate array
        phi_m = self.preallocateMediatorArray()
        # Loop over target and source wavenumbers
        ikyt = self.iky0  # ky is always zero (target is always zonal flows)
        for ikys in range(self.nky):
            # Work out index of mediator
            ikym = ikyt - ikys + self.iky0
            # Check mediator index exists
            if not self.isValidMediatorIndex(ikym, "y"):
                # Just don't set a value to avoid unnecessary cache misses
                continue
            for ikxs in range(self.nkx):
                for ikxt in range(self.nkx):
                    # Work out index of mediator
                    ikxm = ikxt - ikxs + self.ikx0
                    # Check mediator index exists
                    if not self.isValidMediatorIndex(ikxm, "x"):
                        # Just don't set a value to avoid unnecessary cache misses
                        continue
                    # Store mediator value in mediator array
                    phi_m[ikys, ikxs, ikxt, :] = phi[ikym, ikxm, :]
        # Return output array
        return phi_m

    @overrides
    def conj(self, f):
        return np.reshape(np.conj(f[self.iky0, ...]), self.conjShape)

    @overrides
    def getOutputVariableDims(self):
        return ("theta",)

    @overrides
    def getOutputVariableData(self, i):
        return self.results[:, i]
