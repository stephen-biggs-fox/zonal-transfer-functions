#! /bin/bash
# Script to compute net transfer along theta for various times

# NB: Run from /scratch


# Define functions
echo_to_log()
{
    echo $1  # to stdout
    echo "$(date '+%x %X') - $host: $1" >> $shared_log_file
}



# Set constants
shared_dir="${HOME}/Work/zonal-flow-drive"
shared_log_file="$shared_dir/transfer.log"
rm -rf $shared_log_file
touch $shared_log_file
chmod 644 $shared_log_file
work_dir=$(readlink -f .)

# Get machines to use
num_machines=0
machines_to_use=()
total_num_cores=0
tmp_file="load.tmp"
load | tee $tmp_file
cat $tmp_file > $shared_log_file
while read line
do
    machine=$(echo $line | awk '{print $1}')
    free_phys=$(echo $line | awk '{print $9}')
    if [ $(python3 -c "print($free_phys > 1)") == "True" ]
    then
        ((num_machines++))
        machines_to_use+=("$machine")
        cores_to_use+=("$(python3 -c "print(min(int($free_phys), 8))")")
        ((total_num_cores+=$(python3 -c "print(min(int($free_phys), 8))")))
    fi
done < <(cat $tmp_file | tail -n 7)
rm $tmp_file
echo_to_log "Running on $num_machines machines: ${machines_to_use[*]}"
echo_to_log "Using approx. ${cores_to_use[*]} cores, respectively ($total_num_cores total)"

# Loop over machines, giving each one some work
num_times=$(ncdump -h input.out.nc | grep 't = UNLIMITED ; // (.* currently)' | awk '{print $6}' | cut -d "(" -f 2)
num_times_per_core=$((num_times / total_num_cores))
echo_to_log "Approx. $num_times_per_core times per core"
start_index=0
SECONDS=0
for i in $(seq 0 $((num_machines - 1)))  # bash arrays are zero indexed
do
    machine=${machines_to_use[$i]}
    if ((i + 1 == num_machines))  # i.e. last one
    then
        end_index=$((num_times - 1))
    else
        cores=${cores_to_use[$i]}
        times_this_machine=$((cores * num_times_per_core))
        end_index=$((start_index + times_this_machine - 1))
    fi
    echo_to_log "ssh $machine 'mkdir -p $work_dir; cd $work_dir; $shared_dir/run-job.sh $start_index $end_index' &"
    ssh $machine "mkdir -p $work_dir; cd $work_dir; $shared_dir/run-job.sh $start_index $end_index" &
    start_index=$((end_index + 1))
done
wait
echo_to_log "Total time taken: $(seconds_to_time $SECONDS)"
