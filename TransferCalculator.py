"""
Abstract base class to represent the most generalised aspects of a nonlinear transfer calculator

Copyright 2021 Stephen Biggs-Fox

This file is part of zonal-flow-drive.

zonal-flow-drive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

zonal-flow-drive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with zonal-flow-drive.  If not, see http://www.gnu.org/licenses/.
"""


from abc import abstractmethod
from overrides import overrides
from NonlinearCouplingCalculator import NonlinearCouplingCalculator
import numpy as np


class TransferCalculator(NonlinearCouplingCalculator):
    @overrides
    def prepare(self):
        super().prepare()
        self.precompute_k_factors()

    def precompute_k_factors(self):
        self.reshape_k_vectors()
        self.precompute_k_cross_k_prime_dot_z_hat()
        if "v" in self.args.type:
            self.precompute_k_cross_z_hat_dot_z_hat_cross_k_prime()

    def reshape_k_vectors(self):
        for k in ["kyPrime", "kxPrime", "kx"]:
            self.reshape(k)

    def reshape(self, name):
        setattr(
            self,
            name,
            np.reshape(
                getattr(self, name.replace("Prime", "")), getattr(self, name + "Shape")
            ),
        )

    @overrides
    def initializeShapes(self):
        super().initializeShapes()
        # k vector shapes only required for transfer so live here
        # these vary between 3D and 4D so exist here as abstract methods
        # kyShape only required for 4D so lives there
        self.kyPrimeShape = self.getKyPrimeShape()
        self.kxPrimeShape = self.getKxPrimeShape()
        self.kxShape = self.getKxShape()

    @abstractmethod
    def getKyPrimeShape(self):
        "Return tuple of ones as ints with element corresponding to kx dim set to nkx"
        pass

    @abstractmethod
    def getKxPrimeShape(self):
        "Return tuple of ones as ints with element corresponding to kx dim set to nkx"
        pass

    @abstractmethod
    def getKxShape(self):
        "Return tuple of ones as ints with element corresponding to kx dim set to nkx"
        pass

    @abstractmethod
    def precompute_k_cross_k_prime_dot_z_hat(self):
        "Return correctly dimensioned (k x k').z = kx ky' - ky kx'"
        pass

    @abstractmethod
    def precompute_k_cross_z_hat_dot_z_hat_cross_k_prime(self):
        "Return correctly dimensioned (k x z).(z x k') = kx kx' - ky ky'"
        pass

    @overrides(check_signature=False)
    def applyAdditionalComputeCouplingSteps(self, T, *_):
        # All transfer calculations require real part
        return T.real

    @overrides
    def getOutputVariableNamePrefix(self):
        return "T"
