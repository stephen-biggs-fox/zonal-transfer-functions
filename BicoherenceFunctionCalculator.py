"""
Class responsible for calculation of bi-coherence as a function of theta

Copyright 2021 Steve Biggs

This file is part of zonal-flow-drive.

zonal-flow-drive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

zonal-flow-drive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with zonal-flow-drive.  If not, see http://www.gnu.org/licenses/.
"""


from overrides import overrides
from BicoherenceCalculator import BicoherenceCalculator
from FunctionCalculator import FunctionCalculator
import numpy as np


class BicoherenceFunctionCalculator(BicoherenceCalculator, FunctionCalculator):
    @overrides(check_signature=False)
    def applyPerIterationPostProcessing(self, b2, _):
        # Sum to remove k dimensions but use mean so value remains between 0 and 1
        return np.mean(b2)
