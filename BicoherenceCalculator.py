"""
Abstract base class to represent the most generalised aspects of a bi-coherence calculator

Copyright 2021 Stephen Biggs-Fox

This file is part of zonal-flow-drive.

zonal-flow-drive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

zonal-flow-drive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with zonal-flow-drive.  If not, see http://www.gnu.org/licenses/.
"""


from abc import abstractmethod
from overrides import overrides
from NonlinearCouplingCalculator import NonlinearCouplingCalculator
import numpy as np


class BicoherenceCalculator(NonlinearCouplingCalculator):
    @overrides(check_signature=False)
    def applyAdditionalComputeCouplingSteps(self, tripleProduct, f_star, phi_m, f):
        # Pre-calculate numerator and denomenator for safety checks
        numerator = np.abs(self.computeTimeAverage(tripleProduct)) ** 2
        denomenator = self.computeTimeAverage(
            np.abs(f_star) ** 2
        ) * self.computeTimeAverage(np.abs(phi_m * f) ** 2)
        # Check that we can safely ignore divide by zero errors
        assert (numerator[denomenator == 0] == 0).all()
        # Replace zeros in denomenator with ones to avoid divide by zero
        denomenator = np.where(denomenator != 0, denomenator, 1)
        # Now we can do the division safely
        return numerator / denomenator

    @overrides
    def computeTimeAverage(self, f, axis=-1):
        # Override default axis value
        return super().computeTimeAverage(f, axis)

    @overrides
    def getOutputVariableNamePrefix(self):
        return "b2"
