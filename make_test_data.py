#! /usr/bin/env python3
"""
Script to create a small NetCDF file for testing the transfer calculators

Copyright 2021 Steve Biggs

This file is part of zonal-flow-drive.

zonal-flow-drive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

zonal-flow-drive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with zonal-flow-drive.  If not, see http://www.gnu.org/licenses/.
"""


import netCDF4 as nc
import numpy as np


def get_random_array(shape):
    # Fill with random numbers in range -sqrt(2) to sqrt(2) so magnitudes are in range
    # 0 to 1
    return (np.random.rand(*shape) - 0.5) * np.sqrt(2.0)


def write_variable(name, dims, vals):
    var = out.createVariable(name, float, dims)
    var[:] = vals


print("Initializing...")

# Define minimal coordinate arrays
t = np.arange(7.0)
ky = np.arange(3.0)
kx = np.linspace(-1.0, 1.0, num=3)
theta = np.linspace(-np.pi, np.pi, num=9)

# Get / set dimension sizes
nt = len(t)
nspecies = 1
nky = len(ky)
nkx = len(kx)
ntheta = len(theta)
nri = 2

# Seed random number generator so it is random but same every time
np.random.seed(0)

print("Creating data...")

# Create random data arrays of correct size and type
shape = [nt, nky, nkx, ntheta, nri]
phi = get_random_array(shape)
shape.insert(1, nspecies)
ntot = get_random_array(shape)
density = get_random_array(shape)

print("Writing data...")

# Create output file
out = nc.Dataset("test.nc", "w")

# Create dimensions
out.createDimension("t", None)  # t is unlimited dimension
out.createDimension("species", nspecies)
out.createDimension("ky", nky)
out.createDimension("kx", nkx)
out.createDimension("theta", ntheta)
out.createDimension("ri", nri)

# Write dimension variables
write_variable("t", ("t",), t)
write_variable("ky", ("ky",), ky)
write_variable("kx", ("kx",), kx)
write_variable("theta", ("theta",), theta)

# Write data variables
dimensions = ["t", "ky", "kx", "theta", "ri"]
write_variable("phi_t", dimensions, phi)
dimensions.insert(1, "species")
write_variable("ntot_t", dimensions, ntot)
write_variable("density_t", dimensions, density)

# Close file
out.close()

print("Done!")
