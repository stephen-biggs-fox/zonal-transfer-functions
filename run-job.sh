#! /bin/bash
# Script to compute transfer function snapshots for specified time indexes

# Inputs: (** = required)
# $1 = start index (inclusive) **
# $2 = end index (inclusive) **
# $3 = number of cores (total, not per chunk) **
# $4 = chunk size (default 1)
# $5 = output directory (relative to shared_dir)
# $6 = filename offset (default 0)

# Set constants
shared_dir="${HOME}/Work/zonal-flow-drive"
local_log_file="transfer.log"
rm -rf $local_log_file
touch $local_log_file
chmod 644 $local_log_file
# input file must be in /scratch, not $shared_dir, or it won't run properly
# input file can't be in $shared_dir anyway as the file is so large that it fills my $HOME quota
# However, input file can just be a link from /scratch to hwdisks
input_file="input.out.nc"
work_dir=$(readlink -f .)
src=$(echo $work_dir | sed "s|/scratch/snb519/||")
# ln -sf ${HOME}/plasmahwdisks_users/backup/phd/current/$src/$input_file
# export PATH=${HOME}/Programs/Python-3.9.1-install/bin:${HOME}/nmn:$PATH
host=$(cu | tail -n 1 | awk '{print $1}')


# Define functions
echo_to_log()
{
    echo $1  >> $local_log_file
    echo "$(date '+%x %X') - $host: $1" >> $shared_log_file
}


# Parse inputs
if [ -z "$1" ]
then
    echo "ERROR: Must specify start index"
    echo "Exiting..."
    exit 1
else
    istart=$1
fi

if [ -z "$2" ]
then
    echo "ERROR: Must specify stop index"
    echo "Exiting..."
    exit 2
else
    iend=$2
fi

if [ -z "$3" ]
then
    echo "ERROR: Must specify number of cores"
    echo "Exiting..."
    exit 3
else
    nproc=$3
fi

if [ -z "$4" ]
then
    chunk_size=1
else
    chunk_size=$4
fi

if [ -z "$5" ]
then
    out_dir=$shared_dir  # Gets ignored in path
else
    out_dir=$shared_dir/$5
fi

if [ -z "$6" ]
then
    offset=0
else
    offset=$6
fi

shared_log_file="$out_dir/transfer.log"
nproc_per_chunk=$((nproc / chunk_size))

# echo "DEBUG: istart=$istart"
# echo "DEBUG: iend=$iend"
# echo "DEBUG: nproc=$nproc"
# echo "DEBUG: chunk_size=$chunk_size"
# echo "DEBUG: out_dir=$out_dir"
# echo "DEBUG: offset=$offset"
# echo "DEBUG: nproc_per_chunk=$nproc_per_chunk"

# Loop over times
for this_start in $(seq $istart $chunk_size $iend)
do
    # echo "DEBUG: this_start=$this_start"
    this_end=$(python3 -c "print(min([$this_start + $chunk_size - 1, $iend]))")
    # echo "DEBUG: this_end=$this_end"
    if [ $chunk_size -gt 1 ]
    then
        echo_to_log "Calculating T(theta) for timesteps $this_start to $this_end..."
    else
        echo_to_log "Calculating T(theta) at timestep number $this_start..."
    fi
    SECONDS=0
    # Run chunks in parallel
    for t in $(seq $this_start $this_end)
    do
        t_label=$((t + offset))
        out_file="$out_dir/T_theta_$t_label.txt"
        $shared_dir/compute_transfer_function.py $input_file $t $((t + 1)) $nproc_per_chunk $out_file &>> $local_log_file &
    done
    wait
    if [ $chunk_size -gt 1 ]
    then
        echo_to_log "Completed calculating T(theta) for timesteps $this_start to $this_end. Time taken: $(seconds_to_time $SECONDS)"
    else
        echo_to_log "Completed calculating T(theta) at timestep number $this_start. Time taken: $(seconds_to_time $SECONDS)"
    fi
    # Fix out_file permissions
    for t in $(seq $this_start $this_end)
    do
        t_label=$((t + offset))
        out_file="$out_dir/T_theta_$t_label.txt"
        chmod 644 $out_file
    done
    echo ""  >> $local_log_file
done
