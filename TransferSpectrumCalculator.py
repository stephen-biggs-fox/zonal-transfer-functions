"""
Class responsible for calculation of nonlinear transfer as a function of t and theta

Copyright 2021 Steve Biggs

This file is part of zonal-flow-drive.

zonal-flow-drive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

zonal-flow-drive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with zonal-flow-drive.  If not, see http://www.gnu.org/licenses/.
"""


from overrides import overrides
from TransferCalculator import TransferCalculator
from SpectrumCalculator import SpectrumCalculator
import numpy as np


class TransferSpectrumCalculator(TransferCalculator, SpectrumCalculator):
    @overrides
    def extractVariables(self):
        super().extractVariables()
        if self.args.index:
            self.itheta = int(self.args.theta)
        else:
            self.itheta = np.argmin(np.abs(self.theta - self.args.theta))

    @overrides
    def reshape_k_vectors(self):
        super().reshape_k_vectors()
        self.reshape("ky")

    @overrides
    def initializeShapes(self):
        super().initializeShapes()
        #              ky',kx',     ky,kx
        self.kyShape = (1, 1, self.nky, 1)

    @overrides
    def getKyPrimeShape(self):
        #            ky',kx',ky,kx
        return (self.nky, 1, 1, 1)

    @overrides
    def getKxPrimeShape(self):
        #      ky',      kx',ky,kx
        return (1, self.nkx, 1, 1)

    @overrides
    def getKxShape(self):
        #     ky',kx',ky,      kx
        return (1, 1, 1, self.nkx)

    @overrides
    def getConjShape(self):
        #     ky',kx',      ky,       kx
        return (1, 1, self.nky, self.nkx)

    @overrides
    def getMediatorShape(self):
        #             ky',      kx',      ky,       kx
        return (self.nky, self.nkx, self.nky, self.nkx)

    @overrides
    def getSourceShape(self):
        #             ky',      kx',ky,kx
        return (self.nky, self.nkx, 1, 1)

    @overrides
    def precompute_k_cross_k_prime_dot_z_hat(self):
        self.k_cross_k_prime_dot_z_hat = self.kx * self.kyPrime - self.ky * self.kxPrime

    @overrides
    def precompute_k_cross_z_hat_dot_z_hat_cross_k_prime(self):
        self.k_cross_z_hat_dot_z_hat_cross_k_prime = (
            -self.kx * self.kxPrime - self.ky * self.kyPrime
        )

    @overrides
    def getParallelDim(self):
        return "t"

    @overrides
    def get_t_slice(self, i):
        return i

    @overrides(check_signature=False)
    def get_theta_slice(self, _):
        return self.itheta

    @overrides
    def compute_phi_m(self, phi):
        # Pre-allocate array
        phi_m = self.preallocateMediatorArray()
        # Loop over target and source wavenumbers
        for ikys in range(self.nky):
            for ikxs in range(self.nkx):
                for ikyt in range(self.nky):
                    # Work out index of mediator
                    ikym = ikyt - ikys + self.iky0
                    # Check mediator index exists
                    if not self.isValidMediatorIndex(ikym, "y"):
                        # Just don't set a value to avoid unnecessary cache misses
                        continue
                    for ikxt in range(self.nkx):
                        # Work out index of mediator
                        ikxm = ikxt - ikxs + self.ikx0
                        # Check mediator index exists
                        if not self.isValidMediatorIndex(ikxm, "x"):
                            # Just don't set a value to avoid unnecessary cache misses
                            continue
                        # Store mediator value in mediator array
                        phi_m[ikys, ikxs, ikyt, ikxt] = phi[ikym, ikxm]
        # Return output array
        return phi_m

    @overrides
    def conj(self, f):
        return np.reshape(np.conj(f), self.conjShape)

    @overrides
    def applyOverallPostProcessing(self):
        # Compute time average first to remove time dimension
        self.results = self.computeTimeAverage(self.results)
        # Apply 2 * (k x k').z after averageing to minimize number of floating point ops
        self.results *= 2 * self.k_cross_k_prime_dot_z_hat
        # Apply (k x z).(z x k') if necessary
        if "v" in self.args.type:
            i = self.args.type.index("v")
            self.results[i, ...] *= self.k_cross_z_hat_dot_z_hat_cross_k_prime

    @overrides
    def getOutputVariableDims(self):
        return ("ky_prime", "kx_prime", "ky", "kx")

    @overrides
    def getOutputVariableData(self, i):
        return self.results[i, ...]

    @overrides
    def getOutputVariableAttrs(self):
        attrs = super().getOutputVariableAttrs()
        attrs["theta"] = self.theta[self.itheta]
        return attrs
