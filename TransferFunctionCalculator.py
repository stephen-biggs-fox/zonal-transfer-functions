"""
Class responsible for calculation of nonlinear transfer as a function of t and theta

Copyright 2021 Steve Biggs

This file is part of zonal-flow-drive.

zonal-flow-drive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

zonal-flow-drive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with zonal-flow-drive.  If not, see http://www.gnu.org/licenses/.
"""


from overrides import overrides
from TransferCalculator import TransferCalculator
from FunctionCalculator import FunctionCalculator
import numpy as np


class TransferFunctionCalculator(TransferCalculator, FunctionCalculator):
    @overrides
    def getKyPrimeShape(self):
        #            ky',kx',kx
        return (self.nky, 1, 1)  # No t because applied after time averaging

    @overrides
    def getKxPrimeShape(self):
        #      ky',      kx',kx
        return (1, self.nkx, 1)  # No t because applied after time averaging

    @overrides
    def getKxShape(self):
        #      ky',kx',     kx
        return (1, 1, self.nkx)  # No t because applied after time averaging

    @overrides
    def precompute_k_cross_k_prime_dot_z_hat(self):
        # Neglect -ky kx' term as ky = 0
        self.k_cross_k_prime_dot_z_hat = self.kx * self.kyPrime

    @overrides
    def precompute_k_cross_z_hat_dot_z_hat_cross_k_prime(self):
        # Neglect -ky ky' term as ky = 0
        self.k_cross_z_hat_dot_z_hat_cross_k_prime = -self.kx * self.kxPrime

    @overrides
    def applyAdditionalComputeCouplingSteps(self, T, *_):
        # Take real part first to avoid unnecessary computation on imaginary part
        T = super().applyAdditionalComputeCouplingSteps(T)
        # Compute time average next to remove time dimension
        T = self.computeTimeAverage(T, axis=-1)
        # Apply (k x k').z last to minimize number of floating point operations
        T *= self.k_cross_k_prime_dot_z_hat
        return T

    @overrides(check_signature=False)
    def applyPerIterationPostProcessing(self, T, f):
        # Apply (k x z).(z x k') if necessary
        if f == "v":
            T *= self.k_cross_z_hat_dot_z_hat_cross_k_prime
        # Sum next to remove k dimensions
        T = np.sum(T)
        # Times by 2 last to minimise number of floating point operations
        T *= 2
        return T
