"""
Abstract base class to represent the most generalised aspects of a nonlinear coupling calculator

Copyright 2021 Stephen Biggs-Fox

This file is part of zonal-flow-drive.

zonal-flow-drive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

zonal-flow-drive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with zonal-flow-drive.  If not, see http://www.gnu.org/licenses/.
"""


from abc import ABC, abstractmethod
from overrides import EnforceOverrides
import multiprocessing as mp
import netCDF4 as nc
import numpy as np
import os
import utils
import xarray as xr


class NonlinearCouplingCalculator(ABC, EnforceOverrides):
    def __init__(self, args):
        self.args = args

    def calculate(self):
        self.prepare()
        self.do()

    def prepare(self):
        print("Loading data...")
        self.loadData()
        print("Extracting variables...")
        self.extractVariables()
        print("Initializing shapes...")
        self.initializeShapes()

    def loadData(self):
        # TODO - extract data loading into separate class so data can be loaded just
        # once for multiple calculations, e.g. transfer function and bi-coherence
        print("Opening dataset...")
        self.openDataset()
        print("Dropping unrequired data...")
        self.dropUnrequiredData()
        # Load data for performance and to avoid concurrent read problems
        print("Loading arrays into memory...")
        self.ds.load()

    def openDataset(self):
        sep = ","
        if sep in self.args.input:
            input_filename_list = [f for f in self.args.input.split(sep)]
            self.ds = xr.open_mfdataset(input_filename_list)
            # Drop duplicate time entries that sometimes exist after a restart
            _, uniqueTimesMask = np.unique(self.ds["t"], return_index=True)
            self.ds = self.ds.isel(t=uniqueTimesMask)
        else:
            self.ds = xr.open_dataset(self.args.input)

    def dropUnrequiredData(self):
        self.dropUnrequiredVars()
        self.dropUnrequiredTimes()

    def dropUnrequiredVars(self):
        vars_to_delete = self.getVarsToDelete()
        self.ds = self.ds.drop_vars(vars_to_delete)

    def getVarsToDelete(self):
        required_vars = self.getRequiredVars()
        vars_to_delete = self.getUnusedCoords()
        for v in self.ds.data_vars:
            if v not in required_vars:
                vars_to_delete.append(v)
        return vars_to_delete

    def getRequiredVars(self):
        required_vars = ["theta", "kx", "ky", "t", "phi_t"]
        if "n" in self.args.type:
            required_vars.append("ntot_t")
        if "d" in self.args.type:
            required_vars.append("density_t")
        return required_vars

    def getUnusedCoords(self):
        vars_to_delete = []
        for coord in ["egrid", "lambda"]:  # un-used coords
            if (
                coord in self.ds.coords
            ):  # if block to avoid deleting non-existent coords
                vars_to_delete.append(coord)
        return vars_to_delete

    def dropUnrequiredTimes(self):
        if self.args.index:
            istart = int(self.args.start)
            if self.args.end is None:
                iend = len(self.ds["t"])
            else:
                iend = int(self.args.end)
        else:
            istart = np.argmin(np.abs(self.ds["t"].values - self.args.start))
            if self.args.end is None:
                iend = len(self.ds["t"])
            else:
                iend = np.argmin(np.abs(self.ds["t"].values - self.args.end))
        if iend < len(
            self.ds["t"]
        ):  # if block to avoid dropping if going right to the end
            self.ds = self.dropTimes(slice(iend, None))
        if istart > 0:  # if block to avoid dropping if going right from the start
            self.ds = self.dropTimes(slice(istart))

    def dropTimes(self, sel):
        return self.ds.drop_sel(t=self.ds["t"][sel])

    def extractVariables(self):
        for v in self.ds.data_vars:
            setattr(self, v, self.ds[v].values)
        for c in self.ds.coords:
            setattr(self, c, self.ds[c].values)
            if c == "kx":
                self.kx = np.fft.fftshift(self.kx)
                self.ikx0 = np.argmin(np.abs(self.kx))
            elif c == "ky":
                self.ky = np.fft.fftshift(np.concatenate((self.ky, -self.ky[-1:0:-1])))
                self.iky0 = np.argmin(np.abs(self.ky))
            setattr(self, "n" + c, len(getattr(self, c)))

    def initializeShapes(self):
        # 3D: ky', kx', kx, t
        # 4D: ky', kx', ky, kx
        self.conjShape = self.getConjShape()  # varies between 3D and 4D
        self.mediatorShape = self.getMediatorShape()  # varies between 3D and 4D
        self.sourceShape = self.getSourceShape()  # varies between 3D and 4D

    @abstractmethod
    def getConjShape(self):
        "Return tuple of ones as ints with ky, kx, t elements set to nky, nkx, nt"
        pass

    @abstractmethod
    def getMediatorShape(self):
        "Return tuple of ones as ints with ky', kx', ky, kx, t elements set to nky, nkx, nky, nkx, nt"
        pass

    @abstractmethod
    def getSourceShape(self):
        "Return tuple of ones as ints with ky', kx', t elements set to nky, nkx, nt"
        pass

    def do(self):
        msg = "Running calculation"
        if self.args.nproc > 1:
            msg += " on " + str(self.args.nproc) + " processors"
        msg += "..."
        print(msg)
        self.results = self.parallelCompute()
        self.applyOverallPostProcessing()
        print("Writing output...")
        self.writeOutput()

    def parallelCompute(self):
        parallelDim = self.getParallelDim()
        lenParallelDim = getattr(self, "n" + parallelDim)
        with mp.Pool(processes=self.args.nproc) as p:
            results = np.array(
                p.map(
                    self.compute,
                    range(lenParallelDim),
                    lenParallelDim // self.args.nproc,
                ),
                dtype=float,
            )
        # Shape of results is [nparallel, ntypes, ...] if ntypes > 1 or [nparallel, ...]
        # if ntypes = 1. Hence, reshape so shape is [nparallel, 1, ...] when ntypes = 1
        ntypes = len(self.args.type)
        if ntypes == 1:
            shape = list(results.shape)
            shape.insert(1, 1)
            results = np.reshape(results, shape)
        return results

    @abstractmethod
    def getParallelDim(self):
        "Get string name of dimension to parallelize over"
        pass

    def compute(self, indexAlongParallelDim):
        print(
            "Computing coupling for "
            + self.getParallelDim()
            + " = {}".format(
                getattr(self, self.getParallelDim())[indexAlongParallelDim]
            ),
            flush=True,
        )
        # phi_t[t, ky, kx, theta, ri]
        sel = self.phi_t.ndim * [slice(None)]  # list for element assignment
        sel[0] = self.get_t_slice(indexAlongParallelDim)
        sel[3] = self.get_theta_slice(indexAlongParallelDim)
        # convert list to tuple as other sequences are deprecated for slicing but keep
        # list as need to insert an extra element after this
        arrays = {"v": self.phi_t[tuple(sel)]}
        # ntot_t[t, species, ky, kx, theta, ri], density_t[t, species, ky, kx, theta, ri]
        sel.insert(1, 0)
        sel = tuple(sel)  # now we can convert to a tuple permanently
        if "n" in self.args.type:
            arrays["n"] = self.ntot_t[sel]
        if "d" in self.args.type:
            arrays["d"] = self.density_t[sel]
        return self.computeThisIteration(arrays)

    @abstractmethod
    def get_t_slice(self, i):
        "Return slice or int of t to include this iteration"
        pass

    @abstractmethod
    def get_theta_slice(self, i):
        "Return slice or int of theta to include this iteration"
        pass

    # TODO - note that bicoherence only works with a time average
    def computeThisIteration(self, arrays):
        self.prepareArrays(arrays)
        phi_m = self.compute_phi_m(arrays["v"])
        results = []
        for f in self.args.type:
            coupling = self.computeCoupling(arrays[f], phi_m)
            coupling = self.applyPerIterationPostProcessing(coupling, f)
            results.append(coupling)
        return tuple(results)  # Works even if len(results) == 1

    def prepareArrays(self, arrays):
        for k in arrays.keys():
            for f in self.getArrayPreparationFunctions():
                arrays[k] = f(arrays[k])

    def getArrayPreparationFunctions(self):
        "Return an iterable of callables that operate on and return an array"
        # The easiest way is to return a list of functions
        # Additional parameters should be automatically applied
        # To achieve that, wrap the function with its arguments in a Call (see below)
        return [
            utils.ri_to_complex,
            self.makeFull,
            Call(np.fft.fftshift, {"axes": (0, 1)}),
        ]

    def makeFull(self, array):
        # Assumes dimension order is ky, kx or ky, kx, t
        # For concatenated array:
        # - ky slice = -1:0:-1 so that we include non-zero kys in reverse order
        # - kx slice = -1::-1 so that we include all kxs in reverse order but this puts kx = 0
        #     to the end even though we still want it at the start, hence use of
        #     np.roll(..., 1, axis=1) so that kx = 0 is at ikx = 0
        # - np.conj(...) of the reversed and rolled array as the negative kys are the conjugate
        #     of the positive kys
        # - np.concatenate(..., axis=0) so that we concatenate in the ky direction
        sel = self.getMakeFullSlice()
        return np.concatenate(
            (array, np.conj(np.roll(array[sel], 1, axis=1))),
            axis=0,
        )

    def getMakeFullSlice(self):
        return (slice(-1, 0, -1), slice(-1, None, -1))

    @abstractmethod
    def compute_phi_m(self, phi):
        "Return an array of phi values reordered and reshaped to represent phi_m"
        pass

    def preallocateMediatorArray(self):
        return np.zeros(self.mediatorShape, dtype=complex)

    def isValidMediatorIndex(self, i, direction):
        if direction == "y":
            n = self.nky
        elif direction == "x":
            n = self.nkx
        else:
            raise ValueError(
                "Unrecognised direction '{}'. Must be 'x' or 'y'".format(direction)
            )
        return 0 <= i and i < n

    def computeCoupling(self, f, phi_m):
        # Apply conj and source function to get f_star and f
        f_star = self.conj(f)
        f = self.source(f)  # Must do this after conj
        # All couplings require the triple product first
        coupling = self.computeTripleProduct(f_star, phi_m, f)
        # Apply additional steps as required
        coupling = self.applyAdditionalComputeCouplingSteps(coupling, f_star, phi_m, f)
        return coupling

    @abstractmethod
    def conj(self, f):
        "Return required elements of conj(f) reshaped to self.conjShape"
        pass

    def source(self, f):
        return np.reshape(f, self.sourceShape)

    def computeTripleProduct(self, f_star, phi_m, f):
        # This function assumes the inputs have already been reshaped correctly
        return f_star * phi_m * f

    def applyAdditionalComputeCouplingSteps(self, coupling, f_star, phi_m, f):
        # Default implementation is no further processing
        return coupling

    def computeTimeAverage(self, f, axis=0):
        "Computes the time average of an N-D field with time at the specified axis"
        if self.nt == 1:
            sel = f.ndim * [slice(None)]
            sel[axis] = 0
            timeAverage = f[sel]
        else:
            timeAverage = (
                np.trapz(
                    f,
                    self.t,
                    axis=axis,
                )
                / (self.t[-1] - self.t[0])
            )
        return timeAverage

    def applyPerIterationPostProcessing(self, coupling, f):
        # Default implementation is no further processing
        return coupling

    def applyOverallPostProcessing(self):
        # Default implementation is no further processing
        pass

    def writeOutput(self):
        self.openOutput()
        self.writeData()
        self.closeOutput()

    def openOutput(self):
        if os.path.isfile(self.args.output):
            self.output = nc.Dataset(self.args.output, mode="a")
        else:
            self.output = nc.Dataset(self.args.output, mode="w")

    def writeData(self):
        for v in self.getVariables():
            for d in v.dims:
                if not d in self.output.dimensions:
                    d_no_prime = d.replace("_prime", "")
                    self.output.createDimension(d, getattr(self, "n" + d_no_prime))
                    dimArray = getattr(self, d_no_prime)
                    dimVar = self.output.createVariable(d, dimArray.dtype, (d,))
                    dimVar[:] = dimArray
            if not v.name in self.output.variables:
                var = self.output.createVariable(v.name, v.data.dtype, v.dims)
                var[:] = v.data
                if v.attrs is not None:
                    for k in v.attrs.keys():
                        setattr(var, k, v.attrs[k])

    def getVariables(self):
        "Return iterator of OutputVariables (see class below)"
        variables = []
        for i, f in enumerate(self.args.type):
            variables.append(
                self.OutputVariable(
                    self.getOutputVariableNamePrefix()
                    + "_"
                    + f
                    + self.getOutputVariableNameSuffix(),
                    self.getOutputVariableDims(),
                    self.getOutputVariableData(i),
                    self.getOutputVariableAttrs(),
                )
            )
        return variables

    class OutputVariable:
        def __init__(self, name, dims, data, attrs=None):
            self.name = name
            self.dims = dims
            self.data = data
            self.attrs = attrs

    @abstractmethod
    def getOutputVariableNamePrefix(self):
        pass

    def getOutputVariableNameSuffix(self):
        return ""

    @abstractmethod
    def getOutputVariableDims(self):
        pass

    @abstractmethod
    def getOutputVariableData(self, i):
        pass

    def getOutputVariableAttrs(self):
        return {"t_min": self.t[0], "t_max": self.t[-1]}

    def closeOutput(self):
        self.output.close()


# TODO - generalise and move to utils
class Call:
    def __init__(self, fn, kwargs):
        self.fn = fn
        self.kwargs = kwargs

    def __call__(self, arg):
        return self.fn(arg, **self.kwargs)
